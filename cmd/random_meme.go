package main

import (
	"log"
	"math/rand"
	"os"
)

func getRandomMeme(path string) string {
	if entries, err := os.ReadDir(path); err != nil {
		log.Fatal(err)
	} else {
		entry := entries[rand.Intn(len(entries))]
		return entry.Name()
	}
	return ""
}
